#[allow(unused_imports)]
use std::fs;
use std::env;
use std::collections::HashMap;
use std::collections::binary_heap::BinaryHeap;


struct OrbitSystem {
    orbits: HashMap<String, Vec<String>>,
}

impl OrbitSystem {
    fn new(input: Vec<String>) -> OrbitSystem {
        let mut orbits_map = OrbitSystem {
            orbits: HashMap::new(),
        };

        input.iter().map(|orbit| orbit.split(')').collect::<Vec<&str>>()).filter(|orbit| orbit.len() == 2).for_each(|orbit| {
            let from = orbit[0];
            let to = orbit[1];

            orbits_map.orbits.entry(from.to_string())
            .or_insert_with(|| vec![]).push(to.to_string());

            orbits_map.orbits.entry(to.to_string())
            .or_insert_with(|| vec![]).push(from.to_string());
        });

        orbits_map
    }

    pub fn distance(&self, from: String, to: String) -> Option<u32> {

        let mut distances: HashMap<String, u32> = self.orbits.keys().map( |body| {
            (body.clone(), std::u32::MAX)
        }).collect();

        distances.insert(from.clone(), 0);

        let mut heap = BinaryHeap::new();
        heap.push((from,0));

        while let Some((body, dist)) = heap.pop() {
            if body == to {
                return Some(dist);
            }

            if dist > distances.get(&body).unwrap().clone() {
                continue;
            }

            for other_body in self.orbits.get(&body).unwrap() {
                if dist + 1 < distances.get(other_body).unwrap().clone() {
                    distances.insert(body.clone(), dist + 1);
                    heap.push((other_body.clone(), dist + 1));
                }
            }
        }

        None
    }

    pub fn total_sum(&self) -> u32 {
        self.orbits
            .keys()
            .filter_map(|body| self.distance(body.to_string(), "COM".to_string()))
            .sum()
    }
}

fn get_orbits_from_file(filename: &str) -> (u32, u32) {

    let input = fs::read_to_string(filename).expect("Failed to read/open file.");
    let processed_input: Vec<String> = input.split('\n').map(|s|  s.to_string()).collect();

    let system: OrbitSystem = OrbitSystem::new(processed_input);

    (system.total_sum(), system.distance("YOU".to_string(), "SAN".to_string()).unwrap())
}


fn main() {

    let args: Vec<String> = env::args().collect();

    let (result1, result2) = get_orbits_from_file(args[1].as_str());

    println!("Part1: {}", result1);
    println!("Part2: {}", result2 - 2);

}
