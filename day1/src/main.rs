use std::fs;
use std::io::{ BufReader, BufRead, ErrorKind, Error };
use std::env;
use std::time::{Instant};

fn get_input_vec_from_file(filename: &str) -> Vec<u32> {

    let mut output_vec: Vec<u32> = Vec::new();

    let file = match fs::File::open(filename) {
        Ok(f) => f,
        Err(e) => {
            panic!("Could not open file. Error: {}", e);
        }
    };

    let bufreadr = BufReader::new(file);
    for line in bufreadr.lines() {

        let line = match line {
            Ok(l) => l,
            Err(e) => panic!("Wrong parse. {}", e)
        };
        
        let n: u32 = line.trim().parse().map_err(|e| Error::new(ErrorKind::InvalidData, e)).unwrap();

        output_vec.push(n);
    }
    output_vec
}

fn required_fuel(elem: u32) -> u32 {
    if elem / 3 > 1 {
        (elem / 3) - 2
    } else {
        0
    }
}

fn main() {

    let args: Vec<String> = env::args().collect();

    let input_vec: Vec<u32> = get_input_vec_from_file(args[1].as_str());

    // time measure
    let now = Instant::now();

    // part 1
    let mut result: u32 = 0;
    for el in &input_vec {
        result += required_fuel(*el);
    }
    println!("Part 1 Result: {}", result);
    // end part 1

    // part 2
    result = 0;
    for el in &input_vec {
        let mut value: u32 = *el;
        loop {
            value = required_fuel(value);
            if value == 0 {
                break;
            }
            result += value;
        }
    }
    println!("Part 2 Result: {}", result);
    // end part 2

    // end time measure
    println!("{}", now.elapsed().as_nanos());

}
