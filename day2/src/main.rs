use std::env;
use std::fs;
use std::io::Read;

fn get_input_vec_from_file(filename: &str) -> Vec<u32> {

    let mut output_vec: Vec<u32> = Vec::new();

    let mut file = match fs::File::open(filename).expect("Coundlt open file");

    let mut string_from_file: String = String::new();
    file.read_to_string(&mut string_from_file).unwrap();
    string_from_file = string_from_file.replace(",", " ");

    let splited_str: Vec<&str> = string_from_file.split_whitespace().collect();

    for i in splited_str {
        output_vec.push(i.parse().unwrap());
    }

    println!();

    output_vec

}

fn solve_part1(mut input: Vec<u32>) -> u32 {

    input[1] = 12;
    input[2] = 2;

    let mut opcode_position = 0;
    let jump = 4;
    loop {

        if input[opcode_position] == 99 {
            break;
        }

        let first = input[opcode_position + 1] as usize;
        let second = input[opcode_position + 2] as usize;
        let result = input[opcode_position + 3] as usize;

        if input[opcode_position] == 1 {
            input[result] = input[first] + input[second];
        }else if input[opcode_position] == 2 {
            input[result] = input[first] * input[second];
        }

        opcode_position += jump;

    }

    input[0]
    
}

fn solve_part2(mut input: Vec<u32>, verb: u32, noun: u32) -> bool {
    
    input[1] = verb;
    input[2] = noun;

    let mut opcode_position = 0;
    let jump = 4;
    loop {

        if input[opcode_position] == 99 {
            break;
        }

        let first = input[opcode_position + 1] as usize;
        let second = input[opcode_position + 2] as usize;
        let result = input[opcode_position + 3] as usize;

        if input[opcode_position] == 1 {
            input[result] = input[first] + input[second];
        }else if input[opcode_position] == 2 {
            input[result] = input[first] * input[second];
        }

        opcode_position += jump;

    }
    
    if input[0] == 19690720 {
        return true;
    } 

    false
}

fn main() {

    let args: Vec<String> = env::args().collect();
    let input_vec = get_input_vec_from_file(args[1].as_str());
    
    let result = solve_part1(input_vec.clone());
    let mut result2: u32 = 0;

    for i in 1..100 {
        for j in 1..100 {
            let solution = solve_part2(input_vec.clone(), i, j);
            if solution {
                result2 = 100 * i + j;
            }
        }
    }

    println!("Part1: {}", result);
    println!("Part2: {}", result2);
}
