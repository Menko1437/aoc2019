use std::env;
use std::fs;
use std::collections::HashSet;
use std::hash::Hash;
use std::collections::HashMap;
use std::ops::Add;

struct Action {
    dir: u8,
    len: u32,
}

#[derive(Copy, Clone, Hash, Ord, PartialOrd)]
struct Point {
    x: i32,
    y: i32,
}

impl Add for Point {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Point {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl PartialEq for Point {
    fn eq(&self, other: &Self) -> bool {
        self.x == other.x && self.y == other.y
    }
}

impl Eq for Point {}


fn get_paths_from_file(filename: &str) -> Vec<Vec<Action>> {
    
    let input = fs::read_to_string(filename).expect("Failed to read/open file")
                                            .lines()
                                            .map(|line| line.split(',').map(|res| get_action_data(res)).collect()).collect();

    
    input
}

fn get_action_data(strng: &str) -> Action {
    let dir: u8 = strng.as_bytes()[0];
    let len = strng[1..].parse().unwrap();
    
    Action {
        dir: dir,
        len: len,
    }
}

fn get_path(path: &Vec<Action>) -> Vec<Point> {
    let mut output = Vec::new();
    let mut x = 0;
    let mut y = 0;
    for action in path {
        let pathdir = match action.dir {
            b'R' => (0, 1),
            b'L' => (0, -1),
            b'U' => (-1, 0),
            b'D' => (1, 0),
            _ => { panic!("Unknown Path Direction!") }
        };
        for _i in 0..action.len {
            x += pathdir.0;
            y += pathdir.1;
            output.push(Point { x, y });
        }
    };
    output
}


fn solve_pt1(input: &Vec<Vec<Action>>) -> i32 {

    let path0: HashSet<Point> = get_path(&input[0]).drain(..).collect();
    let path1: HashSet<Point> = get_path(&input[1]).drain(..).collect();
    let output: i32 = path0.intersection(&path1).map(| point | point.x.abs() + point.y.abs()).min().unwrap();

    for i in &path0 {
        println!("{} {}", i.x, i.y);
    }

    output
}

fn solve_pt2(input: &Vec<Vec<Action>>) -> u32 {

    let path0: HashSet<Point> = get_path(&input[0]).drain(..).collect();
    let path1: HashSet<Point> = get_path(&input[1]).drain(..).collect();
    let intesections: Vec<&Point> = path0.intersection(&path1).map(| point | point).collect::<Vec<&Point>>();


    0
}


fn main() {

    let args: Vec<String> = env::args().collect();
    let input: Vec<Vec<Action>> = get_paths_from_file(args[1].as_str());

    let res1 = solve_pt1(&input);
    let res2 = solve_pt2(&input);

    println!("Part1: {}", res1);
    println!("Part2: {}", res2);

}
