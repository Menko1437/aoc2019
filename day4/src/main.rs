
// However, they do remember a few key facts about the password:

// It is a six-digit number.
// The value is within the range given in your puzzle input.
// Two adjacent digits are the same (like 22 in 122345).
// Going from left to right, the digits never decrease; they only ever increase or stay the same (like 111123 or 135679).
// Other than the range rule, the following are true:

// 111111 meets these criteria (double 11, never decreases).
// 223450 does not meet these criteria (decreasing pair of digits 50).
// 123789 does not meet these criteria (no double).

fn has_adjecent(mut input: u32) -> bool {
    let mut splited: [u32; 6] = [0; 6];

    for i in (0..6).rev() {
        splited[i] = input % 10;
        input /= 10;
    }

    let mut last: u32 = 0;
    for i in splited.iter() {
        if *i == last {
            return true;
        }
        last = *i;
    }

    false
}

fn never_decrease(mut input: u32) -> bool {
    let mut splited: [u32; 6] = [0; 6];

    for i in (0..6).rev() {
        splited[i] = input % 10;
        input /= 10;
    }

    let mut last: u32 = 0;
    for i in splited.iter() {
        if *i < last {
            return false
        }
        last = *i;
    }

    true
}

// part 2
fn two_adjecent(mut input: u32) -> bool {
    let mut splited: [u32; 6] = [0; 6];

    for i in (0..6).rev() {
        splited[i] = input % 10;
        input /= 10;
    }

    let mut group_counter = 1;
    let mut last: u32 = 0;
    for i in splited.iter() {
        if *i == last {
            group_counter += 1;
        }else if group_counter == 2{
            return true;
        }else {
            group_counter = 1;
        }
        last = *i;
    }
    if group_counter == 2 {
        return true;
    }
    false
}

fn solve_pt1(min: u32, max: u32) -> u32 {

    let mut counter: u32 = 0;
    for i in min..max {
        if has_adjecent(i) && never_decrease(i) {
            counter += 1;
        }
    } 
     
    counter
}

fn solve_pt2(min: u32, max: u32) -> u32 {

    let mut counter: u32 = 0;
    for i in min..max {
        if has_adjecent(i) && never_decrease(i) && two_adjecent(i) {
            counter += 1;
        }
    } 
     
    counter
}

fn main() {

    let (min, max): (u32,u32) = (402328,864247);

    let res1 = solve_pt1(min, max);
    let res2 = solve_pt2(min, max);

    println!("Part1: {}", res1);
    println!("Part2: {}", res2);

}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_for_adjecent() {
        assert_eq!(has_adjecent(112345), true);
        assert_eq!(has_adjecent(123455), true);
        assert_eq!(has_adjecent(123345), true);
        assert_eq!(has_adjecent(123456), false);
    }

    #[test]
    fn test_for_decrease() {
        assert_eq!(never_decrease(123456), true);
        assert_eq!(never_decrease(111111), true);
        assert_eq!(never_decrease(123245), false);
        assert_eq!(never_decrease(123454), false);
    }

    #[test]
    fn test_for_two_adjecent(){
        assert_eq!(two_adjecent(111221), true);
        assert_eq!(two_adjecent(122334), true);
        assert_eq!(two_adjecent(445555), true);
        assert_eq!(two_adjecent(111166), true);
        assert_eq!(two_adjecent(123456), false);
        assert_eq!(two_adjecent(111222), false);
    }

}