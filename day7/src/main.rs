extern crate permutohedron;

use std::fs;
use std::env;
use std::time::Instant;
use std::collections::VecDeque;

use permutohedron::Heap;



fn get_instructions_from_file(filename: &str) -> Vec<i32> {
    let input_str: String = fs::read_to_string(filename).expect("Couldn't open/read file.");
    let inst: Vec<i32> = input_str.trim().split(',').map(|s| s.parse::<i32>().unwrap()).collect();

    inst
}


/// OPCODE 1 Adds together numbers read from two positions and stores the result in a third position.
/// OPCODE 2 Multiplies the two inputs and stores them in third position.
/// OPCODE 3 Takes a single integer as input and saves it to the position given by its only parameter.
/// OPCODE 4 Outputs the value of its only parameter.    
/// OPCODE 5 If the first parameter is non-zero, it sets the instruction pointer to the value from the second parameter. 
/// OPCODE 6 If the first parameter is zero, it sets the instruction pointer to the value from the second parameter.
/// OPCODE 7 If the first parameter is less than the second parameter, it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
/// OPCODE 8 If the first parameter is equal to the second parameter, it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
/// OPCODE 99 Halts the program / stops execution.
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
enum Opcode {
    Add,
    Mult,
    Save,
    Out,
    Jit,
    Jif,
    Lt,
    Equ,
    Halt,
}

impl From<i32> for Opcode {
    fn from(input: i32) -> Self {
        match input % 10 {
            1 => Opcode::Add,
            2 => Opcode::Mult,
            3 => Opcode::Save,
            4 => Opcode::Out,
            5 => Opcode::Jit,
            6 => Opcode::Jif,
            7 => Opcode::Lt,
            8 => Opcode::Equ,
            _ => Opcode::Halt,
        }
    }
}

enum Mode {
    Position,
    Immediate,
}

impl Mode {
    fn first_param(input: i32) -> Self {
        if input / 100 % 10 == 1 {
            Mode::Immediate
        } else {
            Mode::Position
        }
    }
    fn second_param(input: i32) -> Self {
        if input / 1000 == 1 {
            Mode::Immediate
        } else {
            Mode::Position
        }
    }
}


fn solve(mut instructions: Vec<i32>, input_buffer: &mut VecDeque<i32>) -> Vec<i32> {

    let now = Instant::now();

    let mut output: Vec<i32> = Vec::new();
    let ins_clone = instructions.clone();
    let mut i: usize = 0;

    loop{
        let (opcode, mode1, mode2) = parse_opcode(instructions[i]);
        
        if opcode == Opcode::Halt {
            break;
        }

        if now.elapsed().as_secs() == 5 {
            break;
        }

        match opcode {
            Opcode::Add => {

                println!("OPCODE: ADD");

                let parm1 = match mode1 {
                    Mode::Position => {
                        instructions[instructions[i + 1] as usize]
                    },
                    Mode::Immediate => {
                        instructions[i + 1]
                    }
                };
                let parm2 = match mode2 {
                    Mode::Position => {
                        instructions[instructions[i + 2] as usize]
                    },
                    Mode::Immediate => {
                        instructions[i + 2]
                    }
                };
                
                instructions[ins_clone[i + 3] as usize] = parm1 + parm2;
                i += 4;
            },
            Opcode::Mult => {

                println!("OPCODE: MULT");

                let parm1 = match mode1 {
                    Mode::Position => {
                        instructions[instructions[i + 1] as usize]
                    },
                    Mode::Immediate => {
                        instructions[i + 1]
                    }
                };
                let parm2 = match mode2 {
                    Mode::Position => {
                        instructions[instructions[i + 2] as usize]
                    },
                    Mode::Immediate => {
                        instructions[i + 2]
                    }
                };
                
                instructions[ins_clone[i + 3] as usize] = parm1 * parm2;
                i += 4;
            },
            Opcode::Save => {
                println!("OPCODE: SAVE");

                instructions[ins_clone[i + 1] as usize] = match input_buffer.pop_front() {
                    Some(val) => val,
                    None => {0}
                };
               
                i += 2;
            },
            Opcode::Out => {
                println!("OPCODE: OUT");
                let parm1 = match mode1 {
                    Mode::Position => {
                        instructions[instructions[i + 1] as usize]
                    },
                    Mode::Immediate => {
                        instructions[i + 1]
                    }
                };

                output.push(parm1);
                i += 2; 
            },
            Opcode::Jit => {

                println!("OPCODE: JIT");

                let parm1 = match mode1 {
                    Mode::Position => {
                        instructions[instructions[i + 1] as usize]
                    },
                    Mode::Immediate => {
                        instructions[i + 1]
                    }
                };
                
                if parm1 != 0 {
                    i = match mode2 {
                        Mode::Position => {
                            instructions[instructions[i + 2] as usize]
                        },
                        Mode::Immediate => {
                            instructions[i + 2]
                        }
                    } as usize;
                    continue;
                }

            },
            Opcode::Jif => {
                println!("OPCODE: JIF");

                let parm1 = match mode1 {
                    Mode::Position => {
                        instructions[instructions[i + 1] as usize]
                    },
                    Mode::Immediate => {
                        instructions[i + 1]
                    }
                };
                if parm1 == 0 {
                    i = match mode2 {
                        Mode::Position => {
                            instructions[instructions[i + 2] as usize]
                        },
                        Mode::Immediate => {
                            instructions[i + 2]
                        }
                    } as usize;
                    continue;
                }   

            },
            Opcode::Lt => {
                println!("OPCODE: LT");

                let parm1 = match mode1 {
                    Mode::Position => {
                        instructions[instructions[i + 1] as usize]
                    },
                    Mode::Immediate => {
                        instructions[i + 1]
                    }
                };
                let parm2 = match mode2 {
                    Mode::Position => {
                        instructions[instructions[i + 2] as usize]
                    },
                    Mode::Immediate => {
                        instructions[i + 2]
                    }
                };
                
                let output = if parm1 < parm2 { 1 } else { 0 };
                instructions[ins_clone[i + 3] as usize] = output;

                i += 4;
            },
            Opcode::Equ => {
                println!("OPCODE: EQ");

                let parm1 = match mode1 {
                    Mode::Position => {
                        instructions[instructions[i + 1] as usize]
                    },
                    Mode::Immediate => {
                        instructions[i + 1]
                    }
                };
                let parm2 = match mode2 {
                    Mode::Position => {
                        instructions[instructions[i + 2] as usize]
                    },
                    Mode::Immediate => {
                        instructions[i + 2]
                    }
                };
                
                let output = if parm1 == parm2 { 1 } else { 0 };
                instructions[ins_clone[i + 3] as usize] = output;
                i += 4;
            },
            Opcode::Halt => {
                println!("OPCODE: HALT");
                break;
            }
        }

        
    }

    output
}

fn parse_opcode(instruction: i32) -> (Opcode, Mode, Mode) {
    ( 
        Opcode::from(instruction), 
        Mode::first_param(instruction),
        Mode::second_param(instruction)
    )
}

#[allow(unused_variables)]
fn solve_0to4(instructions: Vec<i32>) -> i32 {
    
    let mut nums: Vec<u32> = vec![0,1,2,3,4];
    let permutations = Heap::new(&mut nums);

    let mut max: i32 = 0;

    let mut perms: VecDeque<i32> = VecDeque::new();
    for perm in permutations {
        println!("Permutation: {}, {}, {}, {}, {}", perm[0], perm[1], perm[2], perm[3], perm[4]);
        perms.push_back(perm[0] as i32);
        perms.push_back(0);
        perms.push_back(perm[1] as i32);
        perms.push_back(perm[2] as i32);
        perms.push_back(perm[3] as i32);
        perms.push_back(perm[4] as i32);
        
        let mut intcode_output = solve(instructions.clone(), &mut perms);
        perms.push_front(intcode_output[0]);
        perms.swap(0, 1);
        intcode_output = solve(instructions.clone(), &mut perms);
        perms.push_front(intcode_output[0]);
        perms.swap(0, 1);
        intcode_output = solve(instructions.clone(), &mut perms);
        perms.push_front(intcode_output[0]);
        perms.swap(0, 1);
        intcode_output = solve(instructions.clone(), &mut perms);
        perms.push_front(intcode_output[0]);
        perms.swap(0, 1);
        intcode_output = solve(instructions.clone(), &mut perms);

        if intcode_output[0] > max {
            max = intcode_output[0];
        }
        
    }


    max
}

fn main() -> Result<(), std::io::Error> {

    let args: Vec<String> = env::args().collect();

    let instructions: Vec<i32> = get_instructions_from_file(args[1].as_str());

    let p1 = solve_0to4(instructions.clone());
    println!("PART1: {:?}", p1);

    Ok(())
}